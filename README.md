<blockquote>
<h2><span style="color: #ff6600;"><strong>Hi, I'm<br /></strong></span><span style="color: #ff6600;"><strong>Kirichenko Stanislav<br /></strong></span><span style="color: #ff6600;"><strong>C++ Software developer</strong></span></h2>
</blockquote>
<p>&nbsp;</p>
<h2><span style="color: #ffffff;"><strong>My game engine - OpenGL era</strong></span></h2>
<p>One of many topic I had to face during initial development were 3d models, I had to create a method to import them into a OpenGL context and be able to render them. Shown in the picture below it's a 3D model borrowed from the game "Amnesia - The dark descent", it is stored as a COLLADA format which uses the XML syntax. The XML format was parsed with the help of another library called RapidXML. Everything else was done from scratch using a self-made dynamic link library to abstract the API calls written entirely in C.</p>
<p>The texture were loaded with the implementation of the "DevIL" library. In the top right corner is shown the UV map of the model, and it's rendered using a orthogonal projection onto a framebuffer.</p>
<p><strong><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/ColladaLoader.jpg" alt="" width="700" height="470" /></strong></p>
<p>Here we can see the Crytek's Sponza by&nbsp;Frank Meinl, this time the file's format was OBJ Wavefront, loaded with another self-made method written in C. The scene is composed of 262,267 Triangles and around 100 textures with diffuse, normal and specular maps.</p>
<p>The model was converted to a more friendly custom binary format to increase to loading speed. The format was written in Big-Endian and during loading was converted at run-time depending on the endianess of the architecture it was running on.</p>
<p>The scene lights are rendered using a Deferred Rendering method, in this picture there are <b>200 point lights </b> each with a different color and intensity. Also you can see my text renderer in the top right corner supporting uft8 encoding.</p>
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/ObjLoaderSponza.jpg" alt="" width="850" height="549" /></p>
<p>Here we can see my implementation of multipass PCF (percentage close filtering) shadows in the deferred pipeline. The shaders are written in GLSL language with OpenGL core 4.3 API. Light types implemented are pointlights and spotlights.</p>
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/LightsAndShadows.jpg" alt="" width="700" height="549" /></p>
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/shadwosn.jpg" alt="" width="700" height="549" /></p>
<p>One of the many goals of the project was the integration of the BulletPhysics library and to implement a system to handle dynamic rigid bodies with custom collision shapes.</p>
<p>The engine had also implemented OpenAL for the 3d spatial sounds.</p>
<p>The scene's resources are handled by a "Resource Manager" written using templates to accomodate every type of resource possible, where a resource was not bound to an object anymore it can be safely deleted to free memory.</p>
<p>&nbsp; <a href="https://www.youtube.com/watch?v=cZ2eGk490gA&amp;feature=emb_title"> <img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/MultipleLightsBulletPhysics.jpg" alt="" width="700" height="549" /></a></p>
<p>The entities are data driven, they use the "Entity Component System" pattern, which can handle over 10 milion objects. Each component type will have a unique ID given at compile time, the management of the entity properties can be changed at runtime through the help of LUA scripting.</p>
<p>The scene was loaded from a custom file encoded with the XML style for user readabilty.<br />Also the mesh material attribute are stored in separate files.</p>
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/mapformat.jpg" alt="" width="754" height="650" /></p>
<p>&nbsp;</p>
<p>A font renderer was created using only OpenGL, leading me one step closer to develop my User Interface. It has many special effects such as glow, shadow, outline, underline, 3D extrude. <br />The text style can be saved, edited and restored on the need.<br />The main feature of this implementation is that all the text is batched into a buffer transferred to the GPU using <a href="https://en.wikipedia.org/wiki/Direct_memory_access">DMA</a> and rendered using a single draw call at the end of the frame. The cpu buffer is a custom memory pool class and it tries to use as less memory.<br />It can render&nbsp;<a href="https://en.wikipedia.org/wiki/UTF-8">UTF-8</a> encoded text, with a wide range of supported languages, but for simplicity it lack of an advanced layout system for languages such as hebrew.</p>
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/fnt_Renderer.jpg" alt="" width="700" height="429" /></p>
<p>The font data is parsed using FreeType2 library and all the glyphs are cached onto a big compressed texture such as one below using algorithms like box packing. The class supports bitmap fonts and also <a href="https://steamcdn-a.akamaihd.net/apps/valve/2007/SIGGRAPH2007_AlphaTestedMagnification.pdf">Signed Distance Field</a> fonts. The class also permitted to parse the font at run-time or it could be preprocessed offline once and ready to be loaded from a custom compressed binary format.</p>
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/font_atlas.png" alt="" width="512" height="512" /></p>
<p>&nbsp;</p>
<p>Then it came the basic concept of Graphics User Interfaces totally written from scratch, inspired by the&nbsp; Unreal Engine's "Slate UI Framework". After many and many iterations I came up with something visually appealing and also functional.<br />It had implemented components such as:<br />-Toolbar, Drop down menus<br />-Buttons, Checkboxes, slider,<br />-Border, Label, Image, Viewport<br />-Scrollbar<br />-Horizontal/Vertical/Wrap box, to this day I've refactored this code and still use it in my rewritten game engine.</p>
<p>&nbsp;</p>
<table style="height: 441px;" width="1015">
<tbody>
<tr>
<td style="width: 500.5px;">
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/GuiSimple.jpg" alt="" width="500" height="400" /></p>
</td>
<td style="width: 500.5px;">&nbsp;
<p>&nbsp;</p>
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/GuiSimpleUsage.jpg" alt="" width="500" height="400" /></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;<img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/ui_viewport.jpg" alt="" width="700" height="400" /></p>
<p>&nbsp;<a href="https://youtu.be/HddI8YKftDk"> <img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/t_gui.jpg" alt="" width="560" height="314" /> </a></p>
<p>&nbsp;</p>

<p>I've also written a few openGL wrappers over the years but I still wasn't satisfied with the State machine like api of OpenGL.</p>


<h2>Vulkan days 2020 - today</h2>
<p> Over the years I've rewritten my little game engine with more modern c++ and Vulkan api instead of OpenGL</p>
<p> Now the engine is around 300 files and 380k lines of code all written from scratch, and I'm very proud of what I've achieved.</p>

<p><img src="assets/gameTest.gif" alt="" width="50%" height="50%" /></p>

<h3>Custom vulkan wrapper</h3>
<p>I've written a custom wrapper called <a href="https://github.com/WildFoxStudio/furyrenderer">Fury</a> to handle complex boilerplate of setting up Vulkan, it become easier to work with and I think it has a cleaner API since it works with ids instead of pointers, with which I can check if an object is alive, I still have 0(1) read write complexity and since they are uint32_t are very trivially copyable. </p>
<p><img src="assets/furySponzaBindless.jpg" alt="" width="50%" height="50%" /></p>
<p>Sponza rendered with draw indexed indirect using bindless array of textures using Fury wrapper.</p>


<h3>Figma mockups</h3>
<p>I use figma as my main platform to design UX, mockups and much more.</p>
<p><img src="assets/mockups.jpg" alt="" width="100%" height="100%" /></p>

<h3>Engine progress through the years</h3>
<p>Refining the GUI mockups and improving and refactoring existing code, made the product look more professional and has more feature, such as Projects generation as a separate DLL, Play in Editor, native C++ scripting, ECS, custom binary formats for all resources. Custom gizmo using procedural geometry, Implemented a FSM system for editor tools.</p>
<p><img src="assets/engineProgress.jpg" alt="" width="100%" height="100%" /></p>

<h3>Optimizations</h3>
<p>The first implementation was 20ms per frame then I added batching and reduced to 7ms. I wasn't satisfied with the performance of my GUI system, so I refactored the code to use the MVC pattern with event driven geometry caching in the model using the decorator pattern to handle the widgets customization, geometry batching and micro optimization to the presentation. Further I managed to squeeze it down to 0.01ms and there is still room for optimization but sometimes we need the draw a line for our sanity. </p>
<p><img src="assets/guiOptimization.jpg" alt="" width="100%" height="100%" /></p>

<h3>Automated testing</h3>
<p>I didn't want to have too many dependencies in my project so I wrote my own simple testing library called <a href="https://github.com/WildFoxStudio/Bitter---Testing-Framework"> Bitter </a></p>
<p>For me It does the job done without any external dependencies, it works similar to unreal engine testing framework and it's also inspired by gtest, it runs all the tests by default and outputs cleanly the results.</p>
<p><img src="assets/bitterjpg.jpg" alt="" width="50%" height="50%" /></p>


<h3>Graphics Debugging</h3>
<p>I don't know if I could live without Nvidia Nsight or RenderDoc, they are the most essential tools for graphics debugging.</p>

<h2><strong>Other short-term projects</strong></h2>
<p>This is "Space Invaders" by David Winter running on my self-made Chip8 interpreter written in ANSI C in 2 days. <a href="https://github.com/WildFoxStudio/Chip8-ANSI-C-Interpreter"> Link </a></p>
<table style="height: 32px;" width="439">
<tbody>
<tr>
<td style="width: 212.5px;">
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/2020-07-06.png" alt="" width="500" height="400" /></p>
</td>
<td style="width: 212.5px;">&nbsp;
<p><img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/2020-07-06 (1).png" alt="" width="500" height="400" /></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Here I'm showing an integration of the LUA scripting language in a C++ evironment.</p>
<p>&nbsp;<a href="https://www.youtube.com/watch?v=FS5Xpnxrw2s&amp;feature=emb_title"> <img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/t_lua.jpg" alt="" width="560" height="314" /></a></p>
<p>Here's an implementation of the rendering of a Quadratic Bezier curve written in Python using PyGame and in GLM.</p>
<p>&nbsp;<a href="https://youtu.be/VHsC3jriyOg"> <img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/t_bezier.jpg" alt="" width="560" height="314" /></a></p>
<p>Here is the result of a 2 hours challenge. I build my own version of Minesweeper using Python and PyGame. (2014)</p>
<p>&nbsp;<a href="https://youtu.be/TIyeo5LJtqQ"> <img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/t_minesweeper.jpg" alt="" width="560" height="314" /></a></p>
<p>Yet another challenge, I wrote a Flappy bird clone in 1 hour using Game maker. (2015)</p>
<p>&nbsp;<a href="https://youtu.be/2xlOds6ZhEI"> <img src="https://gitlab.com/RedFoxStudio/RedFoxStudioPortfolio/-/raw/master/assets/t_fp.jpg" alt="" width="560" height="314" /></a></p>
<p>I would like to show you more but unfortunately all the other projects have been lost forever due my computer's hard disk death few years ago.</p>